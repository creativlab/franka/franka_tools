#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Software License Agreement (BSD)

file      move_end_effector.py
authors   adrien guenard <adrien.guenard@loria.fr>
copyright Copyright (c) 2019, LORIA (UMR 7503 CNRS, Universite de Lorraine, INRIA) , All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that
the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the
   following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
   following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of LORIA nor the names of its contributors may be used to endorse or promote
   products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WAR-
RANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, IN-
DIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

"Simple example to control the end effector of Franka robot with cartesian impedance controller, moves end effector along Y axis during 3 seconds"

import rospy

from geometry_msgs.msg import PoseStamped
from franka_msgs.msg import FrankaState

#const variables for topics names
ROBOT_COMMAND_TOPIC="/cartesian_impedance_example_controller/equilibrium_pose"
FRANKA_STATE_TOPIC="/franka_state_controller/franka_states"

class controler:

    #End effector position is base frame (O_T_EE)
    FrankaEE_X=0
    FrankaEE_Y=0
    FrankaEE_Z=0   
    
    #Flag for state receiving
    franka_state_received=False

    def __init__(self):
        rospy.init_node('end_effector_controler') # Init ROS node
        rate=rospy.Rate(50) # Set rate 50hz
        rospy.loginfo('Robot controler started')
        self.link_name = rospy.get_param("~link_name")

        #Subscribers: 
        #Inputs: 
        # state of robot: FrankaState
        self.state_sub = rospy.Subscriber(FRANKA_STATE_TOPIC,FrankaState, self.franka_state_callback)# sub to franka_state

        #Publishers
        #Outputs: 
        #Position of Franka End effector
        self.commands_pub=rospy.Publisher(ROBOT_COMMAND_TOPIC,PoseStamped,queue_size=1)

        self.t0=rospy.Time.now()
        while not rospy.is_shutdown():
            self.control()
            rate.sleep()

    
    def control(self):

        if self.franka_state_received==True:
            t=rospy.Time.now()
            franka_pos_command = PoseStamped()
            franka_pos_command.header.frame_id = self.link_name
            franka_pos_command.header.stamp = t

            franka_pos_command.pose.position.x = self.FrankaEE_X
            franka_pos_command.pose.position.y = self.FrankaEE_Y+0.05*((t.secs-self.t0.secs)<3)
            franka_pos_command.pose.position.z = self.FrankaEE_Z

	    #To set orientation
#            franka_pos_command.pose.orientation.x = ...
#            franka_pos_command.pose.orientation.y = ...
#            franka_pos_command.pose.orientation.z = ...
#            franka_pos_command.pose.orientation.w = ...

            self.commands_pub.publish(franka_pos_command)

    def franka_state_callback(self,msg):# Franka state callback function
        if (self.franka_state_received==False):
            self.franka_state_received=True
        self.FrankaEE_X=msg.O_T_EE[12]
        self.FrankaEE_Y=msg.O_T_EE[13]
        self.FrankaEE_Z=msg.O_T_EE[14]

if __name__ == '__main__':
    try:    
        controler()
    except rospy.ROSInterruptException:
        pass
