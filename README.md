# franka-tools

Versions:

- ROS 1 Noetic
- libfranka 0.9.2
- franka_ros 0.9.0

The repository franka_tools  is available to ease the development on the robot. It contains examples for main libraries and uses a Docker image to run it.
Be aware that the data created in the docker image are not persistent: the docker image will be re-initialized each time it is started. However, the folder in which you run the docker is mounted in the container. The recommended workflow is to clone the franka_tools repository in your home directory on miniPC, create your own controller files in the franka_tools folders, and use the Docker image to run it.
To use franka_tools and run the docker image:

- Log in the Mini PC
- Check that the Mini PC is running the real-time kernel: in a terminal the command

`uname –a`

 should return the kernel version used and must contain the string PREEMPT RT. If not, reboot the miniPC and choose RT kernel in grub startup menu.

- Log in to the gitlab registry:

`docker login registry.gitlab.inria.fr`

- Clone the franka_tools tools repository
- Run launch script in franka_tools directory to run the docker container

Franka_tools repository: https://gitlab.inria.fr/creativlab/franka/franka_tools

 Here are the recommended procedures to develop with the main libraries:

•	**Develop with libfranka**
Some example codes are in the franka_tools/libfranka_examples folder to control the robot with different controllers, or get state variables.
To build the examples or your controller (C++), run in franka_tools/libfranka_examples:

    mkdir build (if not already existing)

    cd build

    cmake -DCMAKE_BUILD_TYPE=Release ..

    cmake --build .

Information on libfranka : https://frankaemika.github.io/docs/libfranka.html

•	**Develop with Cartesian_franka**

To move the robot with this library, simply create a Python script and import pycartesian_franka.
See example.py script in cartesianfranka_example folder in which the robot is moved through a translation and a rotation.

Information on Cartesian_franka: https://github.com/resibots/cartesian_franka

•	**Build and run ROS node**

ROS is already installed in the container, and a catkin workspace is located at /app/catkin_ws.
The franka_ros package is installed in /app/catkin_ws/src
Custom packages can be created in /home/ros_packages_src, which is linked (symbolic link) to catkin workspace in /app/catkin_ws/src/src
To build packages, run:

    cd /app/catkin_ws

    catkin_make

    source /app/catkin_ws/devel/setup.bash

An example ROS package is in /home/ros_packages_src/ee_control_example. It contains a node which uses the cartesian impedance controller to move the end effector along the Y axis.
To run the example:
	`roslaunch ee_control_example ee_control.launch`

Information on ROS and franka_ros:
https://www.ros.org/
https://frankaemika.github.io/docs/franka_ros.html

**Useful hints for ROS**

The docker image launched Terminator terminal emulator which allow to create multiple terminal windows.
Use Ctrl+Shift+O and Ctrl+Shift+E to split terminal window orizontally or vertically
